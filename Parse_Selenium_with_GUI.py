import csv
from selenium import webdriver
from bs4 import BeautifulSoup
from time import sleep
import requests


def read_csv():
    list=[]
    with open('mexico.csv', 'r') as fp:
        reader = csv.reader(fp, delimiter=',', quotechar='"')
        for row in reader:
            yield row


def write_csv(data):
    row = []
    for i in data:
        row.append(data[i])
    row = tuple(row)
    with open('mexico_out.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow(row)


def parse_ad(data):
    link = 'https://www.gympass.com/negocios/{}/info'.format(data[0])
    r = requests.get(link)
    print(data[0])
    soup = BeautifulSoup(r.text, 'lxml')
    try:
        title = soup.find('h1', class_='gym_show_title').text.strip()
    except:
        return {'id': data[0]}
    country = r.text.split('var country = "')[1].split('";\n')[0]
    city = r.text.split('var city = "')[1].split('";\n')[0]
    region = r.text.split('var state_code = "')[1].split('";\n')[0]
    #address = soup.find('div', class_='gym_show_header_address').text.strip().split(' -')[0]
    address = soup.find('div', class_='map-module').get('data-address')
    lat = data[1]
    long = data[2]
    numbers=[]
    phone_num = [i.get('href') for i in soup.find_all('a', class_='call-now-btn')]
    for i in range(len(phone_num)):
        if i == len(phone_num)-1 and phone_num[i] not in numbers:
            numbers.append(phone_num[i])
        elif phone_num[i] != phone_num[i+1]:
            numbers.append(phone_num[i])
    if len(numbers) > 1:
        phones = {'phone1': numbers[0], 'phone2': numbers[1]}
    elif len(numbers) <= 1:
        try:
            phones = {'phone1': numbers[0], 'phone2': ' '}
        except:
            phones = {'phone1': ' ', 'phone2': ' '}

    amenities = [i.find('img').get('alt') for i in soup.find_all('div', class_='amenities-services')]
    if 'D1of63syhvgryrgwgmafd43ccvcekod0' in amenities:
        index = amenities.index('D1of63syhvgryrgwgmafd43ccvcekod0')
        amenities[index] = 'Access to disabled'

    hours = [i.text.strip().replace('\n          \n\n              ', ' ').
                 replace('\n            \n\n              ', ', ')
             for i in soup.find_all('div', class_='hour-row')[:-1]]
    activities = [i.getText().strip() for i in soup.find_all('span', class_='activities-itens')]
    about = [i.text.strip() for i in soup.find('div', class_='about-content').find_all('p')]
    try:
        logo = soup.find('div', class_='gym_header_logo_container').find('img').get('src')
    except:
        logo = ' '
    try:
        photos = [i.get('href') for i in soup.find('div', class_='gyms-photos').find_all('a')]
    except:
        photos = []
    output_data = {'title': title, 'country': country, 'region': region, 'city': city, 'address': address,
                   'lat': lat, 'long': long, 'phone_num_1': phones['phone1'], 'phone_num_2': phones['phone2'],
                   'amenities': amenities, 'hours': hours, 'about': about, 'activities': activities, 'logo': logo,
                   'photos': photos}
    return output_data


def parse_all(url):

    driver = webdriver.Chrome()
    driver.get(url)
    chile = {}

    html = driver.page_source
    soup = BeautifulSoup(html, 'lxml')
    ads_info = soup.find('div', class_='refresh_gyms_info')

    #Тут руками меняем отображение на карте
    # Как автоматизировать - пока думаю

    ads = str(ads_info).split('{&quot;')[1:]
    for i in ads:
        lat = float(i.split('lat&quot;:')[1].split(',&quot;')[0])
        lat = "%.6f" % lat
        long = float(i.split('lng&quot;:')[1].split(',&quot;')[0])
        long = "%.6f" % long
        id = int(i.split('data-id=\'')[1].split('\'/&gt;&quot;')[0])
        chile.update({str(id): {'id': id, 'lat': lat, 'long': long}})

        write_csv({'id': id, 'lat': lat, 'long': long})

    driver.close()


def main():
    url = 'https://www.gympass.com/como_funciona?ll=en'
    data = read_csv()
    for i in data:
        data = parse_ad(i)
        write_csv(data)
    #parse_all(url)


if __name__ == '__main__':
    main()


